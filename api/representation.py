from typing import List

import requests

from .payload import CATEGORIES, PROD_IN_CAT


class LanceRequest:
    """
    LanceRequest is a tool class for representation purposes.
    """

    @staticmethod
    def get_dirty_data(url: str, payload: dict, key_to_access: str) -> dict:
        """
        get_dirty_data get uncleaned data from openfoodfacts API.

        Args:
            url (str): the url address to fetch data from
            payload (dict): infos to submit to server to filter result of query
            key_to_access (str): key to access data inside json response

        Returns:
            dict: a dict of uncleaned data
        """
        return requests.get(url, params=payload).json()[key_to_access]

    @staticmethod
    def filtered_data(dirty_data: list, wanted_data: tuple) -> list:
        """
        filtered_data strip unwanted data from requested data.

        Args:
            dirty_data (list): list of unfiltered data
            wanted_data (tuple): a tuple of wanted data

        Returns:
            list: list of filtered data
        """
        return [{
            key: value
            for key, value in dirty_dict.items() if key in wanted_data
        } for dirty_dict in dirty_data]

    @staticmethod
    def cleaned_data(data_keys: tuple, datas: list) -> list:
        """
        cleaned_data clean requested data.

        Args:
            data_keys (tuple): key of values to be cleaned
            datas (list): list of data to be cleaned

        Returns:
            list: list of cleaned data
        """
        for key in data_keys:
            for data in datas:
                data[key] = data[key].split(",")
        return datas


class OpenFoodApi(LanceRequest):
    """
    OpenFoodApi purpose is to serve data coming from API.

    Args:
        LanceRequete (class): class tool for OpenFoodApi class
    """
    payload_cat = CATEGORIES
    payload_prod = PROD_IN_CAT

    def get_cat_init(self) -> List[dict]:
        """
        get_cat_init get the most important categories from API

        Returns:
            List[dict]: list of filtered categories
        """
        dirty_categories = self.get_dirty_data(
            self.payload_cat['url'],
            self.payload_cat['params'],
            'tags',
        )
        return self.filtered_data(dirty_categories,
                                  self.payload_cat['wanted_data'])

    def get_init_prods(self) -> List[List[dict]]:
        """
        get_init_prods pass products for each initial category into a list

        Args:
            categories (List[dict]): list of filtered categories

        Returns:
            List[List[dict]]: list of list of product for each initial category
        """
        return [
            self.get_prod_related(category['name'])
            for category in self.get_cat_init()
        ]

    def get_prod_related(self, category_name: str) -> List[dict]:
        """
        get_prod_related get some products of a category

        Args:
            category_name (str): name of category

        Returns:
            List[dict]: list of products related to one category
        """
        self.payload_prod['params'].update({"tag_0": category_name})
        dirty_products = self.get_dirty_data(
            self.payload_prod['url'],
            self.payload_prod['params'],
            "products",
        )
        return self.cleaned_data(
            ('categories', ),
            self.filtered_data(dirty_products, self.payload_prod['wanted_data'])
        )
