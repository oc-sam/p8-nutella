Django==4.0.3
requests==2.27.1
django-bootstrap-v5==1.0.11
psycopg2==2.9.3
# test
factory-boy==3.2.1
pytest==7.0.1
pytest-django==4.5.2
pytest-cov==3.0.0
# quality
flake8==4.0.1
# prod
gunicorn==20.1.0
whitenoise==6.2.0