import factory

from users.models import UserProfile


class UserProfileFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = username = factory.LazyAttribute(
        lambda user: f'{user.first_name.lower()}@example.com'
    )

    password = "secret"
    is_staff = False
    is_superuser = False

    class Meta:
        model = UserProfile
