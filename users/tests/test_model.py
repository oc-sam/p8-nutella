from django.core.exceptions import ValidationError
from django.test import TestCase
from substitute.models.product import Product
from users.models import UserProfile


class UserTestCase(TestCase):
    fixtures = ['data.json']

    def test_all_favs_prods(self):
        user_with_favs = UserProfile.objects.first()
        assert all(
            isinstance(elt, Product)
            for elt in user_with_favs.all_favs_prods
        )

    def test_clean(self):
        instance = UserProfile(
            username="not_email", password="test"
        )
        with self.assertRaises(ValidationError):
            instance.save()
