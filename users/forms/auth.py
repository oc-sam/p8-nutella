from django import forms
from django.contrib.auth.forms import AuthenticationForm


class EmailLoginForm(AuthenticationForm):
    username = forms.EmailField(required=True,
                                help_text="Veuillez saisir une adresse email valide.",
                                widget=forms.TextInput(attrs={
                                    "autofocus": True,
                                    'class': 'd-flex'}
                                ),
                                label="Email")
