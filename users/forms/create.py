from django import forms
from django.contrib.auth.forms import UserCreationForm

from users.models import UserProfile


class EmailUserCreationFrom(UserCreationForm):
    username = forms.EmailField(
        required=True,
        help_text="Veuillez saisir une adresse email valide.",
        widget=forms.TextInput(attrs={'class': 'd-flex'}),
        label="Email")

    class Meta:
        model = UserProfile
        fields = ('username', 'password1', 'password2')
