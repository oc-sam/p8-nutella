from django.urls import reverse_lazy
from django.views import generic

from users.forms.create import EmailUserCreationFrom


class EmailSignUpView(generic.CreateView):
    form_class = EmailUserCreationFrom
    success_url = reverse_lazy('users:login')
    template_name = 'users/registration/signup.html'
