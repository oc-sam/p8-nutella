from django.contrib.auth.views import LoginView

from users.forms.auth import EmailLoginForm


class EmailLoginView(LoginView):
    form_class = EmailLoginForm
    template_name = 'users/registration/login.html'
