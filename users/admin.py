from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import UserProfile


@admin.register(UserProfile)
class ProfileAdmin(UserAdmin):
    ordering = ('email',)
    readonly_fields = ('date_joined', 'last_login', 'email')
