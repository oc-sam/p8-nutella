from django.contrib.auth.views import LogoutView
from django.urls import path
from django.views.generic import TemplateView

from users.views.login import EmailLoginView
from users.views.signup import EmailSignUpView

app_name = 'users'

urlpatterns = [
    path('signup/', EmailSignUpView.as_view(), name='signup'),
    path('login/', EmailLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path(
        'account/',
        TemplateView.as_view(template_name='users/account.html'),
        name='account'
    )
]
