from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import models


class UserProfile(AbstractUser):
    email = models.EmailField("email address", unique=True)
    REQUIRED_FIELDS = []

    @property
    def all_favs_prods(self):
        return [fav.product for fav in self.favorites.all()]

    def clean(self):
        try:
            validate_email(self.username)
            self.email = self.username.lower()
        except ValidationError as e:
            raise e
        return super().clean()

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    class Meta:
        ordering = ["id"]
