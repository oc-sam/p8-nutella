# Pur Beurre

[![image](https://img.shields.io/badge/python-3.9.9-brightgreen)](https://www.python.org/)
[![image](https://img.shields.io/badge/django-4.0.3-green)](https://docs.djangoproject.com/en/4.0/releases/4.0.3/)
[![image](https://img.shields.io/badge/Licence-MIT-blue)](https://gitlab.com/oc-sam/p8-nutella/-/blob/main/LICENSE)
[![Code style: djLint](https://img.shields.io/badge/html%20style-djLint-blue.svg)](https://github.com/Riverside-Healthcare/djlint)

Ceci est un projet étudiant réalisé pour le projet 8 du cours Python d'OpenClassrooms.

Le but de ce projet est de développer une plateforme web accessible à tous afin de trouver un substitut sain à un aliment considéré comme "Trop gras, trop sucré, trop salé".

Suivez [ce lien](https://beurreimpur.herokuapp.com/) pour utiliser l'application et profitez-en !

---

## Table des matières  

- [Planification](#planification)  
- [Fonctionnalités](#fonctionnalités)  
- [Installation en local (linux seulement)](#installation-en-local-linux-seulement)  
- [Utilisation en local (linux seulement)](#utilisation-en-local-linux-seulement)  
  - [Accéder au site](#accéder-au-site)
  - [Lancer les tests](#lancer-les-tests)
  - [Générer un rapport de coverage](#générer-un-rapport-de-coverage)
  - [Lancer le contrôle qualité](#lancer-le-contrôle-qualité)
  - [Accéder à un descriptif des commandes make disponibles](#accéder-à-un-descriptif-des-commandes-make-disponibles)
- [License](#license)  

---

## Planification  

Lien vers board airtable:

- [lien public]("https://airtable.com/shrHb4oy61Yg5XBxO")
- [lien privé]("https://airtable.com/appmHxgw49KdVM1Bz?ao=d29ya3NwYWNlQmFzZXM")

---

## Fonctionnalités

- Affichage du champ de recherche dès la page d’accueil.
- La recherche ne doit pas s’effectuer en AJAX.
- Interface responsive.
- Authentification de l’utilisateur : création de compte en entrant un mail et un mot de passe, sans possibilité de changer son mot de passe pour le moment.

---

## Installation en local (linux seulement)

### Prérequis

python 3.9


1. Cloner ce repo

2. Créer un base de donnée postgres:

```shell
sudo -u postgres psql
```
Depuis psql:
```shell
CREATE DATABASE pur_beurre;
CREATE USER sam_bot WITH PASSWORD 'test123';
ALTER ROLE sam_bot SET client_encoding TO 'utf8';
ALTER ROLE sam_bot SET default_transaction_isolation TO 'read committed';
ALTER ROLE sam_bot SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE pur_beurre TO sam_bot;
\q
```

3. Lancer la commande suivante dans un terminal:

```shell
make install
```

4. Monter l’environnement virtuel créer par la commande make:

```shell
source .venv/bin/activate
```

5. Créer un super utilisateur en suivant les instructions de la commande:

```shell
make superuser
```

---
---
## Utilisation en local (linux seulement)

### Accéder au site

1. Lancer le serveur en local

```shell
make run
```

2. Accéder à l'url suivante

```url
http://localhost:8000/

```

### Lancer les tests

```shell
make tests
```

### Générer un rapport de coverage

```shell
make coverage
```

### Lancer le contrôle qualité

```shell
make quality
```

### Accéder à un descriptif des commandes make disponibles

```shell
make help
```

---

## License

**[MIT license](https://gitlab.com/oc-sam/p8-nutella/-/blob/main/LICENCE)**
