from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('users.urls')),
    path('', include('substitute.urls')),
    path(
        'legal/',
        TemplateView.as_view(template_name='legal.html'),
        name='legal'
    )
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
