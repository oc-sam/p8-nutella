from django import forms


class SearchForm(forms.Form):
    search = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Saisir un nom de produit'}))
