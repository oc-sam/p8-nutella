from api.representation import OpenFoodApi
from api.serialization import OpenDeserializer

from django.core.management.base import BaseCommand

from substitute.bulkator.bulcreator import BulkCreator


class Command(BaseCommand):
    help = ('Get data from OpenFoodFacts API and instantiate Category,'
            'Product, Store and related intermediary models.')

    def __init__(self,
                 stdout=None,
                 stderr=None,
                 no_color=False,
                 force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.OpenConnector = OpenFoodApi()
        self.Bulk = BulkCreator()
        self.Serializer = OpenDeserializer

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        api_res = self.OpenConnector.get_init_prods()
        unsaved_instances = self.Serializer(api_res).get_openfood_data()

        p_inst_saved, c_inst_saved, s_inst_saved = self.Bulk.primary(
            unsaved_instances)

        inst_dict = self.Bulk.get_instances(api_res, p_inst_saved,
                                            c_inst_saved, s_inst_saved)

        self.Bulk.intermediaries(inst_dict)
