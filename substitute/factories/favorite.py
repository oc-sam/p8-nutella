import factory

from substitute.factories.product import ProductFactory
from substitute.models.favorite import Favorite
from users.factory import UserProfileFactory


class FavoriteFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserProfileFactory)
    product = factory.SubFactory(ProductFactory)

    class Meta:
        model = Favorite
