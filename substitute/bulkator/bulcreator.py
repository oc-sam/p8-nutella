import contextlib

from substitute.models.category import Category
from substitute.models.product import Product
from substitute.models.store import Store


class BulkCreator:

    def __init__(self) -> None:
        self.all_models = Product, Category, Store
        self.inter_models = {
            'cats': Product.categories.through,
            'stores': Product.stores.through
        }

    def primary(self, unsaved_instances):
        """
        Save the unsaved (amen) model instances.

        Args:
            unsaved_instances (tuple): unsaved model instances.

        Returns:
            list: list of saved instances.
        """
        model_inst_coresp = zip(self.all_models, unsaved_instances)
        saved_inst = []
        for a_model, unsaved_inst in model_inst_coresp:
            a_model.objects.bulk_create(unsaved_inst, ignore_conflicts=True)
            saved_inst.append(
                list(
                    a_model.objects.filter(
                        name__in=[inst.name for inst in unsaved_inst])))
        return saved_inst

    def get_instances(
        self, cleaned_data, p_inst_saved, c_inst_saved, s_inst_saved
    ):
        """
        Get all saved instances by comparing with OFFAPI data.

        Args:
            cleaned_data (list): Cleaned data from OFFAPI.
            p_inst_saved (list): saved instances of Product.
            c_inst_saved (list): saved instances of Category.
            s_inst_saved (list): saved instances of Store.

        Returns:
            dict: dict of saved instances
            (see _create_empty_lists_saved_inst method for structure).
        """
        inst_dict = {}
        for prod_inst in p_inst_saved:
            for prods in cleaned_data:
                for prod in prods:
                    with contextlib.suppress(KeyError):
                        self._create_empty_lists_saved_inst(inst_dict, prod_inst, prod)
                        self._populate_saved_inst(
                            inst_dict, prod_inst, prod, c_inst_saved, s_inst_saved
                        )
        return inst_dict

    def intermediaries(self, inst_dict):
        """
        Create and save intermediary models instances
        by parsing all saved instances of primary models and their relations.

        Args:
            inst_dict (dict): dict of saved instances
        """
        for related in inst_dict.values():
            for related_name in related.keys():
                if related_name != 'prod':
                    self.inter_models[related_name].objects.bulk_create(
                        self._list_unsaved_instances(related_name, related),
                        ignore_conflicts=True)

    @staticmethod
    def _create_empty_lists_saved_inst(inst_dict, prod_inst, prod):
        """
        Creates empty lists in the empty dict regrouping all saved instances.

        Args:
            inst_dict (dict): empty dict.
            prod_inst: Product instance.
            prod (dict): product data from OFFAPI.
        """
        if prod_inst.name == prod['generic_name_fr']:
            inst_dict[prod_inst.name] = {
                'cats': [],
                'stores': []
            }

    @staticmethod
    def _populate_saved_inst(inst_dict, prod_inst, prod, c_inst_saved, s_inst_saved):
        """
        This method returns all instances got from OFFAPI and saved by previous methods.

        Args:
            inst_dict (dict): instances dict
            prod_inst (list): saved Product instances.
            prod (dict): Cleaned Product data from OFFAPI
            c_inst_saved (list): saved Category instances.
            s_inst_saved (list): saved Store instances.
        """
        for saved_instances, data_key, dict_key in [
            (c_inst_saved, 'categories', 'cats'),
            (s_inst_saved, 'stores_tags', 'stores')
        ]:
            for inst in saved_instances:
                if inst.name in prod[data_key]:
                    inst_dict[prod_inst.name][dict_key].append(inst)
            inst_dict[prod_inst.name]['prod'] = prod_inst

    def _list_unsaved_instances(self, related_name, related):
        """
        Instantiate (but not save) intermediary models present in OFFAPI
        data but not in db.

        Args:
            related_name (str): cats or stores (type of instances)
            related (dict): relataed instances

        Returns:
            list: list of unsaved intermediary models instances.
        """
        if related_name == 'cats':
            return [
                self.inter_models[related_name](product=related['prod'],
                                                category=related_cat)
                for related_cat in related[related_name]
            ]
        elif related_name == 'stores':
            return [
                self.inter_models[related_name](product=related['prod'],
                                                store=related_store)
                for related_store in related[related_name]
            ]
