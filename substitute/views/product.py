from django.db.models import Count, Q
from django.shortcuts import get_object_or_404, render
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView

from substitute.forms.search import SearchForm
from substitute.models.product import Product


class SearchResultView(ListView, FormMixin):
    http_method_names = ['get', 'options']
    form_class = SearchForm
    template_name = 'substitute/search_results.html'
    model = Product
    paginate_by = 6

    def get_queryset(self):
        search_text = self.request.GET.get('search', None)
        if search_text:
            return self.get_prod_by_name(search_text)

    @staticmethod
    def get_prod_by_name(search_text):
        return Product.objects.filter(
                Q(name__icontains=search_text.lower().strip())
            ).order_by('-nutriscore')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = self.request.GET.get('search')
        return context


class ProductDetailView(DetailView):
    model = Product

    def get(self, request, *args, **kwargs):
        product = get_object_or_404(Product, pk=kwargs['pk'])
        substitutes = self._refine_substitutes(product, self._get_substitutes(product))
        context = {'product': product, 'substitutes': substitutes}
        return render(request, 'substitute/product_detail.html', context)

    @staticmethod
    def _get_substitutes(product):
        return Product.objects.annotate(
            common_categories=Count(
                "categories",
                filter=Q(categories__in=product.categories.all())
            )
        ).annotate(
            num_categories=Count("categories")
        )

    def _refine_substitutes(self, product, substitutes):
        no_product_categories = product.categories.all().count()
        substitutes.filter(common_categories=no_product_categories)
        if product.nutriscore == "a":
            substitutes = substitutes.filter(nutriscore__lte=product.nutriscore)
        else:
            substitutes = substitutes.filter(nutriscore__lt=product.nutriscore)

        return substitutes.exclude(
            pk=self.kwargs['pk']
        ).order_by(
            "nutriscore", "num_categories", "-common_categories"
        )[:3]
