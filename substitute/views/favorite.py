from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView

from substitute.forms.favorite import FavForm
from substitute.models.favorite import Favorite
from substitute.models.product import Product
from substitute.views.utils import get_msg


class AddFavoriteView(CreateView):
    model = Favorite
    form_class = FavForm

    def post(self, request, *args, **kwargs):
        prod = get_object_or_404(Product, pk=kwargs['pk'])
        form = FavForm(request.POST)
        if form.is_valid():
            unsaved_fav = form.save(commit=False)
            unsaved_fav.user = request.user
            unsaved_fav.save()
            messages.success(request, get_msg(prod, "add", "success"))
        else:
            messages.error(request, get_msg(prod, "add", "fail"))

        return redirect(request.META['HTTP_REFERER'])


class RemoveFavoriteView(DeleteView):
    model = Favorite
    form_class = FavForm

    def post(self, request, *args, **kwargs):
        fav = get_object_or_404(Favorite, product=kwargs['pk'], user=request.user)
        form = FavForm(request.POST)
        if form.is_valid():
            fav.delete()
            messages.success(request, get_msg(fav.product, "delete", "success"))
        else:
            messages.error(request, get_msg(fav.product, "delete", "fail"))
        return redirect(request.META['HTTP_REFERER'])


class FavoritesView(ListView):
    http_method_names = ['get', 'options']
    template_name = 'substitute/favorites.html'
    model = Favorite
    paginate_by = 6

    def get_queryset(self):
        return self.request.user.all_favs_prods
