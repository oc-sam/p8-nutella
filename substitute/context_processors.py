from substitute.forms.search import SearchForm


def search_in_navbar(request):
    return {"searchform": SearchForm()}
