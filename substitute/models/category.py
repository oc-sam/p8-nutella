from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)
    products = models.ManyToManyField("Product",
                                      related_name="categories",
                                      blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name_plural = "categories"
