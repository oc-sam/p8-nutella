from django.test import TestCase
from substitute.models.category import Category
from substitute.models.favorite import Favorite
from substitute.models.product import Product
from substitute.models.store import Store


class SubstituteModelsTestCase(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.prod = Product.objects.first()
        self.cat = Category.objects.first()
        self.fav = Favorite.objects.first()
        self.store = Store.objects.first()

    def test_str_methods(self):
        self.assertEqual(
            (
                str(self.prod),
                str(self.cat),
                str(self.store),
                str(self.fav),
            ),
            (
                self.prod.name,
                self.cat.name,
                self.store.name,
                f"{self.fav.user.username}=>{self.fav.product.name}"
            )
        )
