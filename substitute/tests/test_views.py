from django.test import TestCase
from django.urls import reverse
from substitute.models.product import Product
from substitute.views.product import ProductDetailView
from urllib.parse import unquote


class SearchResultViewTest(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.product = Product.objects.first()
        self.response = self.client.get(
            path=reverse('substitute:search'),
            data={'search': self.product.name}
        )

    def test_status_code(self):
        assert self.response.status_code == 200

    def test_queryset_in_context(self):
        assert self.product in self.response.context_data['product_list']
        assert unquote(
            self.response.request['QUERY_STRING']
        ) == f"search={self.product.name}".replace(" ", "+")


class ProductDetailViewTest(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.product = Product.objects.first()
        self.substitutes1 = ProductDetailView._get_substitutes(self.product)

    def test_get_substitutes(self):
        no_cats = self.substitutes1[0].categories.all().count()

        assert 'common_categories' and 'num_categories' in self.substitutes1[0].__dict__.keys()
        assert self.substitutes1[0].num_categories == no_cats
        assert self.substitutes1[0].common_categories <= self.product.categories.all().count()

    def test_refine_substitutes(self):
        self.response1 = self.client.get(f'/product/{self.product.pk}')
        self.view1 = ProductDetailView()
        self.view1.setup(self.response1)
        self.view1.kwargs['pk'] = self.product.pk
        self.refined_sub1 = self.view1._refine_substitutes(
            self.product, self.substitutes1
        )

        self.bad_product = Product.objects.filter(nutriscore='d').first()
        self.substitutes2 = ProductDetailView._get_substitutes(self.bad_product)
        self.request2 = self.client.get(f'/product/{self.bad_product.pk}')
        self.view2 = ProductDetailView()
        self.view2.setup(self.request2)
        self.view2.kwargs['pk'] = self.bad_product.pk
        self.refined_sub2 = self.view2._refine_substitutes(
            self.bad_product, self.substitutes2
        )

        assert self.product not in self.refined_sub1
        assert self.refined_sub1.count() == 3
        sub1, sub2, sub3 = self.refined_sub1
        assert self.product.nutriscore <= sub1.nutriscore <= sub2.nutriscore <= sub3.nutriscore
        assert sub1.num_categories <= sub2.num_categories <= sub3.num_categories
