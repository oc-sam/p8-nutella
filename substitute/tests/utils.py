class MockRequest:
    pass


class MockUser:
    def has_module_perms(self, app_label):
        return app_label == "modeladmin"


class MockAddUser(MockUser):
    def has_perm(self, perm, obj=None):
        return perm == "modeladmin.add_favorite"
