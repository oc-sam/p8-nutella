from django.contrib import admin

from substitute.models.category import Category
from substitute.models.favorite import Favorite
from substitute.models.product import Product
from substitute.models.store import Store


class FavoriteAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False


admin.site.register([Product, Category, Store])
admin.site.register(Favorite, FavoriteAdmin)
